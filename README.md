# JI-UI

### 当前组件库

#### 低阶组件[无依赖]

- j-image封装好的图片组件
- j-local-popup在点击位置弹出自定义插槽的组件
- j-editor富文本编辑器组件
- j-loading封装了多个纯CSS加载样式

#### 高阶组件[有低阶组件依赖]

- j-course-table高度封装的课程表组件，仅支持指定格式数据
- j-item-table，上边那位的依赖组件
- j-tab-bar底部导航栏