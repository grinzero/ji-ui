// components/Editor/Editor.js
Component({
  externalClasses:['j-class','j-e-class'],
  /**
   * 组件的属性列表
   */
lifetimes:{
  created:function(){
    wx.onKeyboardHeightChange(res => {
      console.log('res',res);
      if (res.height === keyboardHeight) return
      const duration = res.height > 0 ? res.duration * 1000 : 0
      keyboardHeight = res.height
      setTimeout(() => {
        wx.pageScrollTo({
          scrollTop: 0,
          success() {
            that.updatePosition(keyboardHeight)
            that.editorCtx.scrollIntoView()
          }
        })
      }, duration)
    })
  }
},

  properties: {
    height:{
      type:String,
      value:'90vw'
    },
    width:{
      type:String,
      value:'90vw'
    },
    iconSize:{
      type:String,
      value:'48rpx'
    },
    placeholder:{
      type:String,
      value:'想说就说吧，想吐槽就吐槽吧'
    },
    insertImageIcon:{
      type:String,
      value:'https://z3.ax1x.com/2021/06/02/2lkmCT.png'
    },
    toolBarShow:{
      type:Boolean,
      value:false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    colorChooseShow:false,
    color:'black',
    big:false,
    header:null
  },

  /**
   * 组件的方法列表
   */
  methods: {
    onEditorReady() {
      const that = this
      this.createSelectorQuery().select('.editor').context(function (res) {
        that.editorCtx = res.context
      }).exec()
    },
    onStatusChange(e) {
      const formats = e.detail
      this.setData({ formats })
    },
    insertDivider(){
      const that=this;
      that.editorCtx.insertDivider();
    },
    insertImage() {
      const that = this
      wx.chooseImage({
        count: 1,
        success: function (res) {
          that.editorCtx.insertImage({
            src: res.tempFilePaths[0],
            data: {
              id: 'abcd',
              role: 'god'
            },
            width: '80%',
            success: function () {
              console.log('insert image success')
            }
          })
        }
      })
    },
    chooseBack(e){
      console.log(e);
      this.setData({
        colorChooseShow:true
      })
    },
    chooseValue(e){
      let {name}=e.currentTarget.dataset;
      let {value}=e.currentTarget.dataset;
      let valueT=e.detail.value;
      value=(value || valueT);
      if (this.data[name]===value) {
        value=""
      }
      this.editorCtx.format(name,value);
      this.setData({
        [`${name}`]:value
      })
    },
    makeItBig(){
      var that=this;
      this.editorCtx.format('fontWeight',that.data.big?'500':'700');
      this.setData({
        big:!that.data.big
      })

    },
    stopAnimation(){
      this.setData({
        textA:true
      })
    },
    onFocus(e){
      var that=this;
      wx.onKeyboardHeightChange(res=>{
        if (res.height==0) {
          wx.offKeyboardHeightChange()
          that.editorCtx.blur();
        }
      })
      this.setData({
        toolBarShow:true
      })
      this.triggerEvent('onFocus',e);
    },
    onBlur(e){
      let that=this;
      this.setData({
        toolBarShow:false
      })
      that.triggerEvent('onBlur',{delta:e.detail})
    }
  }
})
