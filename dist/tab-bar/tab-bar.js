// components/TabBar/TabBar.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    src:{
      type:Array,
      value:[{
        url:'https://z3.ax1x.com/2021/06/01/2upzKf.png',
        onRed:false
      },{
        url:'https://z3.ax1x.com/2021/06/01/2uCWX6.png',
        onRed:true
      },{
        url:'https://z3.ax1x.com/2021/06/01/2u9aZD.png',
        onRed:false
      }]
    },
    chooseItem:{
      type:Number,
      value:0
    }
  },
  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    onClick(e){
      let {index}=e.currentTarget.dataset;
      this.triggerEvent('onClick', {value:index})
      let aniId="#Q"+index+"";
      this.animate(aniId,[{rotate:10},{rotate:-8},{rotate:7},{rotate:-4},{rotate:0}],800,()=>{
        this.clearAnimation(aniId)
      })
    }
  }
})
