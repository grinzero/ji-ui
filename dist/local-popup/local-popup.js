// components/Localpopup/Localpopup.js
Component({
  options:{
    multipleSlots:true,
    externalClasses: ['out-class'],
  },
  /**
   * 组件的属性列表
   */
  properties: {
    visible:{
      type:Boolean,
      value:false
    },
    position:{
      type:String,
      value:'top'
    },
    pointTowards:{
      type:String,
      value:'bottom'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    left:'25px',
    top:'25px',
    ishow:false,
    backgroundColor:'',
    color:''
  },

  /**
   * 组件的方法列表
   */
  methods: {
    closePopup(e){
      var that=this;
      this.setData({
        visible:false,
        show:false
      })
      setTimeout(()=>{
        that.setData({
          ishow:false
        })
      },400)
    },
    openPopup(e){
      var that=this;
      let detail=e.changedTouches[0];
      var top=detail.clientY;
      var left=detail.clientX;
      var width=0;
      var height=0;
      new Promise((resolve,reject)=>{
        that.setData({
          visible:true,
          ishow:true
        },()=>{
          let o = that.createSelectorQuery().select(".out-class").boundingClientRect(rect => {
            width=rect.width;
            height=rect.height;
          }).exec((res)=>{
            resolve(res[0])
          });
        })
      }).then(res=>{
        if (that.properties.position=='top') {
          that.setData({
            top:top-(height*10)-5,
            left:left-(width*10/2),
            show:true
          })
        }
      })
      
    }
  }
})
