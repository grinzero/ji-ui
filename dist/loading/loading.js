// components/loading/loading.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    name:{
      type:String,
      value:'Spinner'
    },
    color:{
      type:String,
      value:'#456afe'
    },
    time:{
      type:String,
      value:'2s'
    },
    size:{
      type:String,
      value:'120rpx'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
