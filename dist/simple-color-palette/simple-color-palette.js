// components/simple-color-palette/simple-color-palette.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    width:{
      type:String,
      value:'40vw'
    },
    color:{
      type:Array,
      value:['white','black','blue','yellow',
              'red','green','blueviolet','darkgrey',
            'mediumslateblue','orange','lightgreen','lightsalmon']
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    chooseColor(e){
      let {color}=e.currentTarget.dataset;
      this.triggerEvent('onClick', {value:color})
    }
  }
})
