// components/course-table/course-table.js
Component({
  options:{
    multipleSlots:true,
  },
  /**
   * 组件的属性列表
   */
  lifetimes:{
    attached:function(e){
      var that=this;
      console.log(this.data)
    }
  },
  properties: {
    sourceData:{
      type:Object,
      value:{}
    },
    tableColorRules:{
      type:Object,
      value:{}
    },
    tableStyle:{
      type:String,
      value:
      `background-color: #ffffff;
      background-image:  linear-gradient(#eee .5px, transparent 0.4px), linear-gradient(to right, #eee .5px, #ffffff 0.4px);
      background-size: calc(95vw / 7) calc(90vh / 12);`
    },
    weekMax:{
      type:Number,
      value:18
    },
    nowWeek:{
      type:Number,
      value:0
    },
    nowWeekTop:{
      type:Number,
      value:0
    },
    showNone:{
      type:Boolean,
      value:false
    },
    homePage:{
      type:String,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    clickCourse:function(e){
      console.log(e)
    },
    indexChange:function(e){
      console.log(e)
      let {current}=e.detail;
      this.setData({
        nowWeekTop:current
      })
    }
  }
})
