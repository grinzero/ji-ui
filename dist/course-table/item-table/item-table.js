// components/course-table/item-table/item-table.js
Component({
  externalClasses:['itemTable'],
  options:{
    multipleSlots:true,
  },
  lifetimes:{
    attached(){
      console.log(this.data)
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {
    courseInfos:{
      type:Object,
      value:{}
    },
    tableColorRules:{
      type:Object,
      value:{}
    },
    weekMax:{
      type:Number,
      value:18
    },
    dayMax:{
      type:Number,
      value:7
    },
    width:{
      type:String,
      value:'100vw'
    },
    height:{
      type:String,
      value:'100vh'
    },
    nowWeek:{
      type:Number,
      value:0
    },
    showNone:{
      type:Boolean,
      value:false
    },
    showNoneChoose:{
      type:Boolean,
      value:true
    },
    maxSection:{
      type:Number,
      value:12
    },
    style:{
      type:String,
      value:''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    indexChange(e){
       this.triggerEvent('indexChange',e.detail)
    },
    clickCourse(e){
      let {dataset}=e.currentTarget;
      this.triggerEvent('clickCourse',dataset);
    },
    longPressTable(e){
      let that=this;
      this.setData({
        showNone:!that.data.showNone
      })
    }
  }
})
