// components/Image/Image.js
Component({
  externalClasses:['j-class'],
  lifetimes:{
    attached:function(){
      var that=this;
      if(this.data.size){
        this.setData({
          width:that.data.size,
          height:that.data.size
        })
      }
    }
  },
  /**
   * 组件的属性列表
   */
  properties: {
    src:{
      type:String,
      value:""
    },
    mode:{
      type:String,
      value:"scaleToFill"
    },
    lazyLoad:{
      type:Boolean,
      value:false
    },
    webp:{
      type:Boolean,
      value:false
    },
    showMenuByLongPress:{
      type:Boolean,
      value:false
    },
    height:{
      type:String,
      value:""
    },
    width:{
      type:String,
      value:""
    },
    style:{
      type:String,
      value:''
    },
    tapFunction:{
      type:String,
      value:'onClick'
    },
    navigateData:{
      type:Object,
      value:{
        appId:null,
        path:'../../packageCourseTable/pages/index/index.'
      }
    },
    size:{
      type:String,
      value:''
    }

  },

  /**
   * 组件的初始数据
   */
  data: {
    visible:false
  },

  /**
   * 组件的方法列表
   */
  methods: {
    loadImage(e){
      this.setData({
        visible:true
      })
      this.triggerEvent('LoadSuccess',null);
    },
    previewImage(e){
      let {src}=e.currentTarget.dataset;
      console.log(src);
      wx.previewImage({
        current:src,
        urls: [src],
      })
    },
    onClick(e){
      this.triggerEvent('onClick',{event:e});
    },
    navigateTo(){
      let that=this;
      if (!this.data.navigateData) {
        return;
      }
      if (this.data.navigateData.appId) {
        that.navigateToMiniProgram({
          appId: that.data.navigateData.appId,
          extraData:that.data.navigateData.extraData,
          path:that.data.navigateData.path
        })
      }else{
        wx.navigateTo({
          url:that.data.navigateData.path,
          fail:e=>{
            wx.reLaunch({
              url: that.data.navigateData.path,
            })
          }
        })
      }
    }
  }
})
